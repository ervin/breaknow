/* This file is part of Break Now

   Copyright 2022 Kevin Ottens <ervin@kde.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3 or any later version
   accepted by the membership of KDE e.V. (or its successor approved
   by the membership of KDE e.V.), which shall act as a proxy
   defined in Section 14 of version 3 of the license.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: root
    property color transparentShade: Qt.rgba(0.25, 0.25, 0.25, 0.5)
    property int remainingTime: initialTimerDuration

    function padded(value) {
        return String(value).padStart(2, "0")
    }

    function formatTime(time) {
        let minutes = Math.floor(time / 60)
        let seconds = time % 60
        if (minutes >= 60) {
            let hours = Math.floor(minutes / 60)
            return hours + ":" + padded(minutes % 60) + ":" + padded(seconds)
        } else {
            return padded(minutes) + ":" + padded(seconds)
        }
    }

    Timer {
        interval: 1000
        repeat: true
        running: root.remainingTime > 0
        onTriggered: root.remainingTime = root.remainingTime - 1
    }

    LinearGradient {
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0.0; color: "black" }
            GradientStop { position: 0.1; color: transparentShade }
            GradientStop { position: 0.85; color: transparentShade }
            GradientStop { position: 0.95; color: "black" }
            GradientStop { position: 1.0; color: "black" }
        }
    }

    Text {
        id: timerText
        text: root.formatTime(root.remainingTime)
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        font.pixelSize: root.height / 8

        SequentialAnimation on color {
            running: root.remainingTime <= 0
            loops: Animation.Infinite
            ColorAnimation {
                from: "black"
                to: "red"
                duration: 1000
                easing.type: Easing.InQuad
            }
            ColorAnimation {
                from: "red"
                to: "black"
                duration: 1000
                easing.type: Easing.OutQuad
            }
        }

        SequentialAnimation on scale {
            running: root.remainingTime <= 0
            loops: Animation.Infinite
            PropertyAnimation {
                from: 1
                to: 1.2
                duration: 500
                easing.type: Easing.OutElastic
            }
            PropertyAnimation {
                from: 1.2
                to: 1
                duration: 500
                easing.type: Easing.InOutQuad
            }
        }
    }

    InnerShadow {
        anchors.fill: timerText
        radius: 8
        samples: 16
        horizontalOffset: -3
        verticalOffset: 3
        color: "white"
        source: timerText
        scale: timerText.scale
    }

}
